/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hmm;

/**
 *
 * @author hp2
 */
public class HMM {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
              
      int numStates = 3;
      
      // TRANSITION MATRIX (a)
      double a[][] = new double [numStates][numStates];
      // STATES name: X,Y,Z
      
        a[0][0] = 0.3;
        a[0][1] = 0.5;
        a[0][2] = 0.2;
        
        
        a[1][0] = 0.1;
        a[1][1] = 0.6;
        a[1][2] = 0.3;
        
        
        a[2][0] = 0.1;
        a[2][1] = 0.4;
        a[2][2] = 0.5;
        
        
        // Emission Matrix (b)
        int numEmisions = 3; 
        // EMISIONS NAme : 0,1,2
        double b [][] = new double [numStates][numEmisions];
        b[0][0] = 0.2;
        b[0][1] = 0.4;
        b[0][2] = 0.4;
        b[1][0] = 0.5;
        b[1][1] = 0.4;
        b[1][2] = 0.1;
        b[2][0] = 0.1;
        b[2][1] = 0.4;
        b[2][2] = 0.5;
        
        
        //OBSERVATION (o)
        int numObservations=3;
        int o [] = new int [numObservations];
        o [0] = 2;
        o [1] = 1;
        o [2] = 2;
        
        //Probability Initial (pi):
        double pi [] = new double [numStates];
        pi [0] = 0.4;
        pi [1] = 0.2;
        pi [2] = 0.3;
        
        // CALCULATING FORWARD PROBABILITIES:
        double[][] f = new double[numStates][o.length];
        for(int l=0; l< f.length; l++) {
            f[l][0] = pi[l] * b[l][o[0]];
            
        }
        for (int i = 1; i < o.length; i++) { 
            for (int k = 0; k < f.length; k++) { 
                double sum = 0; 
                for (int l = 0; l < numStates; l++) { 
                    sum += f[l][i-1] * a[l][k]; 
                    
                } 
                f[k][i] = sum * b[k][o[i]];
                
            }
            
        }
       
       // Print Forward probabilities:
       System.out.println(" FORWARD PROBABILITIES");
        for (int i=0;i<numStates;i++){
            for (int j=0 ; j<o.length ; j++){
               System.out.print(" "+f[i][j]);
            }
            System.out.println("");
        }
    }
    
}
